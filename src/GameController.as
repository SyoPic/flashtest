package 
{
	import flash.display.Sprite;
	import flash.events.Event;

	public class GameController extends Sprite
	{
		private var playing:Boolean = false;
		
		public function GameController() {
			// init containers
			LoggerSingleton.instance.DEBUG("Init containers");
		}
		
		/**
		 * Start game
		 */
		public function start(level:int):void {
			clear();
			init(level);
			resume();
		}
		
		/**
		 * Pause game
		 */
		public function pause():void {
			playing = false;
			LoggerSingleton.instance.DEBUG("Pause game");
		}
		
		/**
		 * Resume game
		 */
		public function resume():void {
			playing = true;
			LoggerSingleton.instance.DEBUG("Resume game");
		}
		
		
		/**
		 * Init game
		 */
		private function init(level:int):void {
			LoggerSingleton.instance.DEBUG("Init level " + level);
			// put level and game objects to stage
			// init input
			// loop
			this.addEventListener(Event.ENTER_FRAME, update);
		}
		
		/**
		 * Main loop
		 */
		public function update(event:Event):void	{
			if (playing) {
				LoggerSingleton.instance.DEBUG("------------");
				LoggerSingleton.instance.DEBUG("Check inputs");
				// check inputs 
				// update model
				LoggerSingleton.instance.DEBUG("Update model");
				// lock input
				// render() -> update game objects positions
				LoggerSingleton.instance.DEBUG("Render level");
				// unlock input
				// check for win/game over -> win() : gameOver()
			}
		}
				
		/**
		 * Win
		 */
		private function win():void {
			pause();
			// show win ui window
		}
		
		/**
		 * Win
		 */
		private function gameOver():void {
			pause();
			// show game over ui window
		}
		
		/**
		 * Clear game
		 */
		public function clear():void {
			pause();
			this.removeEventListener(Event.ENTER_FRAME, update);
			// clear input listeners
			// clear containers & remove game objects
			
			LoggerSingleton.instance.DEBUG("Clear");
		}
		
		/**
		 * Singleton
		 */
		private static var _instance:GameController;
		
		public static function get instance():GameController {
			if (_instance == null) {
				_instance = new GameController();
			}
			return _instance;
		}
	}
}