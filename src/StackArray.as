package
{
	public class StackArray
	{
		private var stacks:Vector.<Vector.<int>> = new Vector.<Vector.<int>>();
		public function StackArray(num:int = 10)
		{
			for (var i:int = 0; i < num; i++) 
			{
				var stack:Vector.<int> = new Vector.<int>();
				stacks.push(stack);
			}
		}
		
		public function push(index:int, value:int):void {
			if (index < stacks.length) {
				stacks[index].unshift(value);
			}
		}
		
		public function pop(index:int):int {
			var result:int = -1;
			if (index < stacks.length) {
				result = stacks[index].shift();
			}
			return result;
		}
		
		public function isEmpty(index:int):Boolean {
			var result:Boolean = true;
			if (index < stacks.length) {
				result = stacks[index].length == 0;
			}
			return result;
		}
		
		public function size(index:int):int {
			var result:int = 0;
			if (index < stacks.length) {
				result = stacks[index].length;
			}
			return result;
		}
	}
}