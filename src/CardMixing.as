package 
{
	public class CardMixing
	{
		private var deck:Array = new Array(52);
		private var suits:Array = ["S", "H", "D", "C"];
		private var ranks:Array = ["Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King"];
		private var shuffleCards:Array = new Array();
		
		public function CardMixing() {}
		
		public function init():void {
			//init cards
			var incr:int = 0;
			
			for (var j:int = 0; j < ranks.length; j++){
				for (var i:int = 0; i < suits.length; i++)
				{
					deck[incr] = ranks[j] + "_" + suits[i];
					LoggerSingleton.instance.DEBUG((incr + 1) + " : " + deck[incr]);
					incr ++;
				}
			}
			shuffleCards = new Array(deck.length);
		}
		
		public function mix():void {
			LoggerSingleton.instance.DEBUG("------------------------");
			init();
			LoggerSingleton.instance.DEBUG("------------------------");
			shuffle();
			deal();
		}
		
		private function shuffle():void {
			var remainingCards:int = deck.length;
			for (var i:int = 0; i < shuffleCards.length; i++) {
				var randomPos:int = (int)(Math.random()*deck.length);
				shuffleCards[i] = deck.splice(randomPos, 1);
			}
		}
		
		private function deal():void {
			for (var i:int = 0; i < shuffleCards.length; i++) {
				LoggerSingleton.instance.DEBUG(shuffleCards[i]);
			}
		}
		
		/**
		 * Singleton
		 */
		private static var _instance:CardMixing;
		
		public static function get instance():CardMixing {
			if (_instance == null) {
				_instance = new CardMixing();
			}
			return _instance;
		}
	}
}