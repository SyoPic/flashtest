package
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.sampler.getSize;
	import flash.utils.Timer;
	
	
	
	
	public class FlashTest extends Sprite
	{
		public function FlashTest() {
			addEventListener(Event.ADDED_TO_STAGE, addedHandler);
		}
		
		private function addedHandler(e:Event):void {
			LoggerSingleton.instance.traceLevel = 1;
			testDaysNum();
			testCardMixing();
			testStackArray();
			testGameLoop(); 
		}
		
		private function testDaysNum():void {
			LoggerSingleton.instance.INFO("------------");
			LoggerSingleton.instance.INFO("Test DaysNum");
			
			LoggerSingleton.instance.DEBUG(DateUtils.getDaysNum(1));
			LoggerSingleton.instance.DEBUG(DateUtils.getDaysNum(2));
			LoggerSingleton.instance.DEBUG(DateUtils.getDaysNum(2, true));
			
			// check obj size
			var sa:StackArray = new StackArray(5);
			LoggerSingleton.instance.DEBUG("getSize " + getSize(sa));
			sa = null;
			LoggerSingleton.instance.DEBUG("getSize " + getSize(sa));
		}
		
		private function testGameLoop():void {
			LoggerSingleton.instance.INFO("--------------");
			LoggerSingleton.instance.INFO("Test Game Loop");
			
			GameController.instance.start(1);
			
			// game loop timer
			var t:Timer = new Timer(100, 1);
			t.addEventListener(TimerEvent.TIMER_COMPLETE, function():void {GameController.instance.clear();});
			t.start();
		}
		
		private function testCardMixing():void {
			LoggerSingleton.instance.INFO("----------------");
			LoggerSingleton.instance.INFO("Test Card Mixing");
			
			CardMixing.instance.mix();
		}
		
		private function testStackArray():void {
			LoggerSingleton.instance.INFO("----------------");
			LoggerSingleton.instance.INFO("Test Stack Array");
			var sa:StackArray = new StackArray(5);
			
			sa.push(0, 101);
			sa.push(0, 102);
			sa.push(0, 103);
			sa.push(0, 104);
			sa.push(0, 105);
			sa.push(0, 106);
			LoggerSingleton.instance.DEBUG("Size 0: " + sa.size(0));
			
			while (sa.size(0) > 0) {
				LoggerSingleton.instance.DEBUG(sa.pop(0));
			}
			
			LoggerSingleton.instance.DEBUG("Stack 0 is emty: " + sa.isEmpty(0));
			
			sa.push(1, 101);
			sa.push(2, 102);
			sa.push(1, 103);
			sa.push(2, 104);
			sa.push(1, 105);
			sa.push(2, 106);
			LoggerSingleton.instance.DEBUG("Size 1: " + sa.size(1));
			
			while (sa.size(1) > 0) {
				LoggerSingleton.instance.DEBUG(sa.pop(1));
			}
			LoggerSingleton.instance.DEBUG("Stack 2 is emty: " + sa.isEmpty(2));
		}
		
	}
}