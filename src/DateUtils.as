package 
{
	public class DateUtils
	{
		private static var monthesArray:Array = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
		
		public static function getDaysNum(month:int, isIntercalary:Boolean = false):int {
			var result:int = -1;
			if (month > 0 && month < monthesArray.length + 1)
				result = (monthesArray[month - 1]);
			if (isIntercalary && month == 2) 
				result ++;
			return result;
		}
	}
}
