package {
	import flash.utils.getTimer;

	public class LoggerSingleton {
		
		private var _traceLevel:int = 0;
		
		public function set traceLevel(level:Number):void {
			_traceLevel = level;
		}
		
		/**
		 * Different levels
		 */
		public function INFO(str:* = ""):void {
			append(str, "INFO", 1);
		}
		
		public function DEBUG(str:* = ""):void {
			append(str, "DEBUG", 2);
		}
		
		public function WARNING(str:* = ""):void {
			append(str, "WARNING", 3);
		}
		
		public function ERROR(str:* = ""):void {
			append(str, "ERROR", 4);
		}
		
		private function append(str:*, prefix:String, tLevel:int):void {
			if (traceLevel <= tLevel)
				trace("[" + getTime() + "] " + prefix + ": " + str);
		}
		
		/**
		 * Get time since launch
		 */
		private function getTime():String {
			return (getTimer() / 1000).toFixed(3);
		}
		
		/**
		 * Singleton
		 */
		private static var _instance:LoggerSingleton;
		
		public static function get instance():LoggerSingleton {
			if (_instance == null) {
				_instance = new LoggerSingleton();
			}
			return _instance;
		}
	}
}


